# MotifNet

This is a slow but effective proof of concept implementation of the self-similar sequence model we call *MotifNet* from the paper:

C. Walder, D. Kim.
Neural Dynamic Programming for Musical Self Similarity.
ICML 2018.
https://arxiv.org/pdf/1802.03144.pdf

## Sample Installation

- Create a python3.6 virtualenv
- Install pytorch (CPU only is sufficient): https://pytorch.org/
- Install the required packages:
    - Symbolic Music Dataset: https://bitbucket.org/cwalder/smd
    - A tedious helper package: https://bitbucket.org/cwalder/gridtools
    - This package: https://bitbucket.org/cwalder/motif

In general with virtualenv, you install by finding the `setup.py` and running it with the *e.g.* `develop` option. For example, for `gridtools`:
```
git clone git@bitbucket.org:cwalder/gridtools.git
cd gridtools/gridtools-master/
python setup.py install
```

## Prepare a dataset

- To create *e.g.* the `NOT` (Nottingham) dataset from the paper, go 

```python data/cleaner.py --dataset_str "['not']" --path "'/tmp/cleaner'"```

- check that it worked: ``ls /tmp/cleaner/not``

## Compare MotifNet with the LSTM:

- Run three variants quickly with the `quick=True` option:

```
python model/motifnet/train.py --model "'lstm'" --dataset_str "'/tmp/cleaner/not'" --quick True
python model/motifnet/train.py --model "'motifnet'" --dataset_str "'/tmp/cleaner/not'" --quick True
python model/motifnet/train.py --model "'motifnetpluslstm'" --dataset_str "'/tmp/cleaner/not'" --quick True
```

- Once it works try with ``quick=False``, but beware that
    - it's slow (some algorithmic improvements and GPU utilisation is required)
    - the hyper-parameters are only roughly tuned.
- Look for *final test_cost* in the printout and compare with the table in the paper linked above.

## Run the model on a general sequence dataset

- If you run the following:

```
python model/motifnet/train.py --model "'lstm'" --dataset_str "'/tmp/my_dataset_word.txt'"
python model/motifnet/train.py --model "'lstm'" --dataset_str "'/tmp/my_dataset_char.txt'"
```

it will detect the ```_word.txt``` and ```_char.txt``` endings and treat the data as either 

- whitespace delimited text (e.g. "the cat sat on the mat"), or
- character level data (e.g. "GATCGATCGTC"), respectively. 

In both cases each line of the ```.txt``` datafile is an example sequence. *Train / valid / test* splits are constructed automatically.