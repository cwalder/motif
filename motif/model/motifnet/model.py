# the main motifnet logic

import numpy as np
import time
import copy
import pandas as pd
from collections import Counter

import gridtools.helpers as gh

import torch
import torch.autograd as autograd
import torch.nn as nn
from torch.nn.parameter import Parameter
import torch.nn.functional as F

import motif.model.motifnet.helpers as dmmmh


# parent class for sequence models
class Model(nn.Module):

    @classmethod
    def get_model(cls, config, midi_hi, **kw):

        assert config.lm or config.motif

        if not config.motif:
            return LM(config, midi_hi, **kw)
        else:
            return MotifNet(config, midi_hi, **kw)

    def __init__(self, config, midi_hi, **kw):

        super(Model, self).__init__()

        self.config = config
        self.midi_hi = midi_hi
        self.word_embeddings = nn.Embedding(midi_hi+1, self.config.embedding_size)
        self.init_lstm_state = tuple(autograd.Variable(torch.zeros(self.config.lstm_layers, 1, self.config.hidden_size), requires_grad=False) for _ in range(2))

        self.drop = nn.Dropout(self.config.dropout)
        self.optimizer = None
        self.info = dict()

        if isinstance(self.config.num_threads, int):
            torch.set_num_threads(self.config.num_threads)
        elif self.config.num_threads is None:
            pass
        else:
            raise Exception('set_num_threads')

    def forward(self, sentence):

        raise NotImplementedError

    def make_optimizer(self):

        kw = dict(params=self.trainable_parameters)
        if self.config.adam_learning_rate is not None:
            kw['lr'] = self.config.adam_learning_rate
        if self.config.weight_decay > 0:
            kw['weight_decay'] = self.config.weight_decay

        self.optimizer = torch.optim.Adam(**kw)

    def make_lstm(self):

        return nn.LSTM(self.config.embedding_size, self.config.hidden_size, num_layers=self.config.lstm_layers, dropout=self.config.dropout)

    def run_epoch(self, dataset, split, shuffle):

        loss_function = nn.NLLLoss()

        n_sentences_total = dataset.n_data(split)
        sum_loss = 0.0
        n_words = 0
        n_sentences = 0
        start_time = time.time()

        if split == 'train':
            self.train()
        else:
            self.eval()

        ptrue_list = []

        self.infos = []

        for isentence, sentence in enumerate(dataset.raw_data(split, shuffle)):

            sentence_tensor = autograd.Variable(torch.LongTensor(sentence))

            scores = self(sentence_tensor[:-1])
            self.infos.append(self.info)

            loss = loss_function(scores, sentence_tensor)
            if split == 'train':
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()

            sum_loss += float(loss)
            n_words += len(sentence)
            n_sentences += 1
            mean_loss = sum_loss / n_sentences

            if (n_sentences == n_sentences_total) or (split == 'train' and ((n_sentences_total < 10) or (n_sentences % (n_sentences_total // 10) == 10))):
                print("%.3f %s average_cost: %s speed: %s wps %s sps" % (n_sentences / n_sentences_total, split, gh.pretty_number(mean_loss), gh.pretty_number(n_words / (time.time() - start_time)), gh.pretty_number(n_sentences / (time.time() - start_time))))

        return mean_loss

    def signature(self):

        data = []
        for k, v in self.state_dict().items():
            vnp = v.numpy()
            data.append((k, vnp.shape, vnp.flatten().min(), vnp.flatten().max(), vnp.flatten().mean()))

        return pd.DataFrame(data=data, columns=['name', 'shape', 'min', 'max', 'mean'])

    def save(self, path):

        print(gh.dump(dict(model=self, state_dict=self.state_dict()), path))

    @classmethod
    def load(cls, path):

        dumped = gh.load(path)
        model = dumped['model']
        model.load_state_dict(dumped['state_dict'])

        return model

    @property
    def trainable_parameters(self):

        return [p for p in self.parameters() if p.requires_grad]

    def run_sample(self, path):

        return None


# simple RNN
class LM(Model):

    def __init__(self, config, midi_hi, **kw):

        super(LM, self).__init__(config, midi_hi)

        self.initial_logits = Parameter(torch.Tensor(1, self.midi_hi))
        nn.init.constant_(self.initial_logits, 0.0)
        self.wout = nn.Linear(self.config.hidden_size, midi_hi)
        self.lstm = self.make_lstm()

    def forward(self, sentence):

        n = len(sentence)
        embeds = self.word_embeddings(sentence)
        embeds_drop = self.drop(embeds)
        lstm_out, _ = self.lstm(embeds_drop.view(n, 1, self.config.embedding_size), self.init_lstm_state)
        out_logits = self.wout(lstm_out.view(n, self.config.hidden_size))
        initial_logits_clamped = dmmmh.leaky_clamp(self.initial_logits, min=self.config.initial_logits_clamp_min, max=self.config.initial_logits_clamp_max, leakage=self.config.initial_logits_clamp_leakage)
        out_logits = torch.cat((initial_logits_clamped, out_logits), 0)
        out_scores = F.log_softmax(out_logits, dim=1)

        return out_scores


# edit tree node
class Node(object):

    counter = Counter()

    def __init__(self, data, symmetrise):

        self.data = data
        self.children = dict()
        self.symmetrise = symmetrise

    def get_child(self, transition, cost, adder, scorer):

        k = self.__transition_to_key(transition)
        child = self.children.get(k, None)

        if child is None:
            Node.counter['new'] += 1
            d_p, score_p = self.data
            this_d = adder(d_p, cost)
            this_score = scorer(this_d)
            child = self.__add_child(k, (this_d, this_score))
        else:
            Node.counter['reuse'] += 1

        return child

    def __add_child(self, transition, data):
        k = self.__transition_to_key(transition)
        return self.children.setdefault(k, Node(data=data, symmetrise=self.symmetrise))

    def __transition_to_key(self, transition):
        if self.symmetrise:
            return tuple(sorted(transition))
        else:
            return tuple(transition)

    @classmethod
    def reset_counter(cls):
        Node.counter.clear()


# motifnet
class MotifNet(Model):

    # edit tree
    class EditPaths(object):

        def __init__(self, sentence, embed, model):

            
            self.sentence_tuple = tuple(sentence.data.numpy())
            self.model = model
            self.n = len(sentence)

            self.root = Node(data=(self.model.init_d, self.model.init_score), symmetrise=self.model.config.symmetrise_costs)
            Node.reset_counter()
            self.NODE = dmmmh.placeholder((self.n, self.n, self.model.config.l_max))

            self.use_tree = self.model.config.hard_max and self.model.config.n_tree_expand > 0
            if self.use_tree:
                self.QUEUES = tuple(dmmmh.SortedMapping() for _ in range(self.model.config.l_max))

            self.C_match = dict()
            self.C_del = dict()
            s_unique = tuple(set(self.sentence_tuple))
            if self.model.config.symmetrise_costs:
                for i in range(self.n):
                    si = self.sentence_tuple[i]
                    if si not in self.C_del:
                        self.C_del[si] = self.model.deleter(dmmmh.pseudo_huber_0p5(embed[i, :]))
                    for j in range(self.n):
                        sj = self.sentence_tuple[j]
                        if (si, sj) not in self.C_match:
                            self.C_match[(si, sj)] = self.C_match[(sj, si)] = self.model.matcher(dmmmh.pseudo_huber_0p5(embed[i, :] - embed[j, :]))
            else:
                for i in range(len(s_unique)):
                    si = s_unique[i]
                    if si not in self.C_del:
                        self.C_del[si] = self.model.deleter(embed[i, :])
                    for j in range(len(s_unique)):
                        sj = s_unique[j]
                        if (si, sj) not in self.C_match:
                            self.C_match[(si, sj)] = self.model.matcher(embed[i, :] - embed[j, :])

        @property
        def node_counter(self):
            global node_counter
            return node_counter

        def print_queues(self):
            if self.use_tree:
                for k, Q in enumerate(self.QUEUES):
                    if len(Q.d):
                        print('k = %i : %s : %s' % (k, str(Q.w), ' '.join(map(str, Q.d.items()))))

        def update_queues(self, i, j, k):
            if self.use_tree:
                node = self.NODE[i, j, k]
                self.QUEUES[k].insert(k=node, w=node.data[1].data.cpu().numpy()[0])

        def get_queue_position(self, k, node):
            return self.QUEUES[k].get_index(node, reverse=True)

        def get_queue_n(self, k):
            return self.QUEUES[k].n

        def extend(self, i, j, k, parent_ind_list):

            if k == 0:

                assert len(parent_ind_list) == 0

                parent = self.root

                si, sj = self.sentence_tuple[i], self.sentence_tuple[j]
                transition = (si, sj)
                cost = self.C_match[(si, sj)]
                self.NODE[i, j, k] = parent.get_child(transition, cost, self.model.adder, self.model.scorer)
                self.update_queues(i, j, k)

            else:

                assert len(parent_ind_list) != 0

                node_candidates = []

                for parent_ind in parent_ind_list:

                    i_p, j_p, k_p = parent_ind
                    node_p = self.NODE[i_p, j_p, k_p]

                    if dmmmh.is_empty(node_p):
                        assert self.use_tree
                        continue

                    if self.use_tree:
                        if self.get_queue_n(k_p) < self.model.config.n_tree_expand:
                            make_child = True
                        else:
                            i_queue = self.get_queue_position(k_p, node_p)
                            make_child = i_queue <= self.model.config.n_tree_expand
                    else:
                        make_child = True

                    if make_child:
                        if i_p == i or j_p == j:
                            assert (i_p != i) or (j_p != j)
                            i_del = i if j_p == j else j
                            assert i_del >= 0
                            s_del = self.sentence_tuple[i_del]
                            transition = (s_del, )
                            cost = self.C_del[s_del]
                        else:
                            assert i == (i_p + 1) and j == (j_p + 1)
                            assert i >= 0 and j >= 0
                            s_i = self.sentence_tuple[i]
                            s_j = self.sentence_tuple[j]
                            transition = (s_i, s_j)
                            cost = self.C_match[(s_i, s_j)]

                        node_candidates.append(node_p.get_child(transition, cost, self.model.adder, self.model.scorer))

                if len(node_candidates) == 0:
                    pass
                else:
                    if len(node_candidates) == 1:
                        self.NODE[i, j, k] = node_candidates[0]
                    else:
                        d_candidates, unnormalised_score_candidates = zip(*[_.data for _ in node_candidates])
                        unnormalised_score_candidates_mat = torch.stack(unnormalised_score_candidates, 0)
                        if self.model.config.hard_max:
                            score_max, i_max = torch.max(unnormalised_score_candidates_mat, 0)
                            i_max = int(i_max)
                            self.NODE[i, j, k] = node_candidates[i_max]
                        else:
                            normalised_score_candidates_mat = self.model.score_normaliser(unnormalised_score_candidates_mat).view(-1)
                            d_candidates_mat = torch.stack(d_candidates, 0)
                            d_new = torch.matmul(normalised_score_candidates_mat, d_candidates_mat)
                            score_new = self.model.scorer(d_new)
                            self.NODE[i, j, k] = Node((d_new, score_new), symmetrise=self.model.config.symmetrise_costs)
                    self.update_queues(i, j, k)

    def __init__(self, config, midi_hi, **kw):

        super(MotifNet, self).__init__(config, midi_hi, **kw)

        self.init_d = autograd.Variable(torch.zeros(self.config.hidden_size), requires_grad=False)
        self.initial_logits = Parameter(torch.Tensor(1, self.midi_hi))
        nn.init.constant_(self.initial_logits, 0.0)

        self.matcher = dmmmh.make_ff(
            [self.config.embedding_size] * self.config.matcher_layers +
            [self.config.hidden_size],
            final_nonlinearity=True,
            drop=self.drop,
            initial_drop=False,
        )

        if self.config.separate_deleter:
            self.deleter = dmmmh.make_ff(
                [self.config.embedding_size] * self.config.matcher_layers +
                [self.config.hidden_size],
                final_nonlinearity=True,
                drop=self.drop,
                initial_drop=False,
            )
        else:
            self.deleter = self.matcher

        self.scorer = dmmmh.make_ff(
            [self.config.hidden_size] * self.config.scorer_layers +
            [1],
            final_nonlinearity=False,
            drop=self.drop,
            initial_drop=True,
        )
        self.score_normaliser = torch.nn.Softmax(0)

        if self.config.ablate_shared_scorer:
            self.output_scorer = dmmmh.make_ff(
                [self.config.hidden_size] * self.config.scorer_layers +
                [1],
                final_nonlinearity=False,
                drop=self.drop,
                initial_drop=True,
            )
        else:
            self.output_scorer = None

        self.analogiser_mapping = dmmmh.make_ff(
            [self.config.hidden_size + self.config.embedding_size] * self.config.analogiser_layers +
            [self.config.embedding_size],
            final_nonlinearity=True,
            drop=self.drop,
            initial_drop=True,
        )

        if self.config.lm:
            out_dimension = self.config.embedding_size + self.config.hidden_size
            self.lstm = self.make_lstm()
        else:
            out_dimension = self.config.embedding_size

        self.w_z = torch.nn.Linear(self.config.hidden_size+self.config.hidden_size, self.config.hidden_size)
        nn.init.xavier_uniform_(self.w_z.weight)
        nn.init.constant_(self.w_z.bias, 0.0)
        self.w_r = torch.nn.Linear(self.config.hidden_size+self.config.hidden_size, self.config.hidden_size)
        nn.init.xavier_uniform_(self.w_r.weight)
        nn.init.constant_(self.w_r.bias, 0.0)
        self.w_h = torch.nn.Linear(self.config.hidden_size+self.config.hidden_size, self.config.hidden_size)
        nn.init.xavier_uniform_(self.w_h.weight)
        nn.init.constant_(self.w_h.bias, 0.0)

        self.sigmoid = nn.Sigmoid()
        self.tanh = nn.Tanh()

        self.final_layer = dmmmh.make_ff(
            [out_dimension] * self.config.out_layers +
            [midi_hi],
            final_nonlinearity=False,
            drop=self.drop,
            initial_drop=True,
        )

    def adder(self, d, c):

        return self.GRU(state=d, input=c)

    def GRU(self, state, input):

        input_drop = self.drop(input).view(-1)
        cat1 = torch.cat([state, input_drop.view(-1)])
        z = self.sigmoid(self.w_z(cat1))
        r = self.sigmoid(self.w_r(cat1))
        cat2 = torch.cat([r * state, input_drop.view(-1)])
        state_tilde = self.tanh(self.w_h(cat2))
        state_new = (1-z) * state + z * state_tilde

        return state_new

    def analogiser(self, d, e):

        return self.analogiser_mapping(torch.cat((d, e), 1))

    def forward(self, sentence):

        self.info = dict()

        n = len(sentence)
        embeds = self.drop(self.word_embeddings(sentence))

        if self.config.lm:
            lstm_out, _ = self.lstm(embeds.view(n, 1, self.config.embedding_size), self.init_lstm_state)
            lstm_out = lstm_out.view(n, self.config.hidden_size)

        self.init_score = self.scorer(self.init_d)

        paths = MotifNet.EditPaths(sentence, embeds, self)

        # we lazily perform a triple loop rather than properly exploit the edit tree:

        for i in range(n):

            for k in range(min(i + 1, self.config.l_max)):

                if k == 0:

                    for j in range(i+1):

                        paths.extend(i, j, k, [])

                else:

                    paths.extend(i, 0, k, [(i - 1, 0, k - 1)])

                    for j in range(1, i):

                        parents = [(i - 1, j - 1, k - 1), (i - 1, j, k - 1), (i, j - 1, k)]
                        paths.extend(i, j, k, parents)

                    j = i
                    parents = [(i - 1, j - 1, k - 1), (i, j - 1, k)]
                    paths.extend(i, j, k, parents)

        out_embeddings = []
        out_embedding_init = self.analogiser(self.init_d.view(1, -1), embeds[[0], :])

        for i in range(n):

            ss = [self.init_score]
            ee = [out_embedding_init]

            for k in range(min(i+1, self.config.l_max)):

                this_j_list = [j for j in range(i) if not dmmmh.is_empty(paths.NODE[i, j, k])]

                if len(this_j_list):

                    this_d_list, this_s_list = map(list, zip(*[paths.NODE[i, j, k].data for j in this_j_list]))
                    this_j_next_list = [j + 1 for j in this_j_list]

                    if self.config.ablate_shared_scorer:
                        this_s_list = [self.output_scorer(this_d) for this_d in this_d_list]

                    this_d_mat = torch.stack(this_d_list, 0)
                    s_next = embeds[this_j_next_list, :]
                    this_e_mat = self.analogiser(this_d_mat, s_next)

                    ee.append(this_e_mat)
                    ss.extend(this_s_list)

            ss = torch.cat(ss, 0)
            ss = self.score_normaliser(ss)
            ee = torch.cat(ee, 0)
            ee = torch.matmul(ss.view(1, -1), ee)
            out_embeddings.append(ee)

        out_embeddings = torch.cat(out_embeddings, 0)

        if self.config.lm:
            out_embeddings = torch.cat((out_embeddings, lstm_out), 1)

        out_logits = self.final_layer(out_embeddings)
        initial_logits_clamped = dmmmh.leaky_clamp(self.initial_logits, min=self.config.initial_logits_clamp_min, max=self.config.initial_logits_clamp_max, leakage=self.config.initial_logits_clamp_leakage)
        out_logits_with_initial = torch.cat((initial_logits_clamped, out_logits), 0)
        out_scores_with_initial = F.log_softmax(out_logits_with_initial, dim=1)

        return out_scores_with_initial
