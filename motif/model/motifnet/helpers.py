# various helpers

import numpy as np
import bisect
import torch
import torch.nn
import torch.nn.init


# feedforward neural network in pytorch
def make_ff(dims, final_nonlinearity, drop, initial_drop, nonlinearity='leaky_relu'):

    if nonlinearity == 'leaky_relu':
        nonlinearity_fn = torch.nn.LeakyReLU
    else:
        raise Exception(nonlinearity)

    gain = torch.nn.init.calculate_gain(nonlinearity, param=None)

    final_i = len(dims)-2
    layers = []
    if initial_drop and drop is not None:
        layers.append(drop)
    for i, (d1, d2) in enumerate(zip(dims[:-1], dims[1:])):
        lin = torch.nn.Linear(d1, d2)
        torch.nn.init.xavier_uniform_(lin.weight, gain=gain)
        torch.nn.init.constant_(lin.bias, 0.0)
        layers.append(lin)
        final = i == (len(dims)-2)
        if final and not final_nonlinearity:
            break
        if drop is not None:
            layers.append(drop)
        layers.append(nonlinearity_fn())

    assert i == final_i

    return torch.nn.Sequential(*layers)


# neural net non-linearity
def leaky_clamp(x, min, max, leakage):
    return torch.clamp(x, min=min, max=max) + leakage * x


# neural net non-linearity
def pseudo_huber_0p5(x):
    return 0.5 * ((1 + 4 * x ** 2) ** 0.5 - 1)


# priority queue helper for lazily expanding the edit tree
class SortedMapping(object):

    def __init__(self):
        self.w = []
        self.n = 0
        self.d = {}

    def insert(self, k, w):

        old_w = self.d.get(k, None)
        if old_w is None:
            self.d[k] = w
            bisect.insort_left(self.w, w, 0, self.n)
            self.n += 1
        else:
            assert old_w == w

    def get_index(self, k, reverse=False):

        w = self.d[k]
        i = bisect.bisect_right(self.w, w) - 1
        return i if reverse is False else self.n - i - 1

    def __len__(self):
        return len(self.w)


# hacky placeholder stuff:

__empty = object()


def is_empty(x, __empty=__empty):
    return id(x) == id(__empty)


def placeholder(shape, __empty=__empty):
    return np.array([__empty] * np.prod(shape), dtype=object).reshape(shape)


def placeholder_apply(x, fn, default=np.nan, dtype=float, __empty=__empty):
    return np.array([fn(_) if not is_empty(_) else default for _ in x.flatten()], dtype=dtype).reshape(x.shape)


def placeholder_is_empty(x):
    return placeholder_apply(x, lambda _: False, default=True, dtype=int)
