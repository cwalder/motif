# hyper-parameter settings for motifnet

import inspect, sys


def assert_nonnegative_integer(x):
    assert isinstance(x, int) and x >= 0


def assert_positive_integer(x):
    assert isinstance(x, int) and x > 0


def assert_boolean(x):
    assert isinstance(x, bool)


class DefaultConfig(object):

    def __init__(self):

        self.hidden_size = 512
        self.embedding_size = 512

        self.lm = True # include a simple RNN?
        self.lstm_layers = 2 # layers of simple RNN

        self.motif = True # include motifnet?
        self.l_max = 4 # max depth of edit tree (larger = slower but more accurate)
        self.matcher_layers = 2 # layers of feed forward matching function
        self.scorer_layers = 2 # layers of feed forward scoring function
        self.analogiser_layers = 2 # layers of feed forward analogiser function
        self.out_layers = 2 # layers of feed forward output layer function
        self.hard_max = True # use a hard max for the generalised edit distance recurrence
        self.n_tree_expand = 0 # max rank of child node in edit tree to expand (larger = slower but more accurate)
        self.symmetrise_costs = True # symmetrise matching cost with pseudo huber
        self.separate_deleter = True # if false use matching function for deletion as well
        self.ablate_shared_scorer = False # don't share output layer scorer and edit distance scorer parameters

        # regularisation
        self.dropout = 0.1
        self.weight_decay = 1e-6
        self.initial_logits_clamp_min = -3
        self.initial_logits_clamp_max = 3
        self.initial_logits_clamp_leakage = 0.0001

        # optimisation
        self.max_strikes = 4
        self.max_train_strikes = 3
        self.max_epoch = 50
        self.adam_learning_rate = 0.001

        # data subsetting etc
        self.allowed_parts = None
        self.data_subset_ratio_pieces = None
        self.data_subset_ratio_events = None
        self.data_subset_proportion_pieces_shortest = None
        self.data_modulo_midi = None
        self.transpose_train = None

        # cpu cores
        self.num_threads = None

    @classmethod
    def flags(cls):
        return tuple(sorted(cls().__dict__.keys()))

    def assert_valid(self):
        assert_nonnegative_integer(self.n_tree_expand)


class LSTMConfig(DefaultConfig):

    def __init__(self):

        DefaultConfig.__init__(self)

        self.motif = False
        self.lm = True


class MotifNetConfig(DefaultConfig):

    def __init__(self):

        DefaultConfig.__init__(self)

        self.motif = True
        self.lm = False


class MotifNetPlusLSTMConfig(DefaultConfig):

    def __init__(self):

        DefaultConfig.__init__(self)

        self.motif = True
        self.lm = True


def get_config(model, overrides=None):

    if model == "lstm":
        r = LSTMConfig()
    elif model == "motifnet":
        r = MotifNetConfig()
    elif model == "motifnetpluslstm":
        r = MotifNetPlusLSTMConfig()
    else:
        raise ValueError("Invalid model: %s", model)

    r.name = model

    r = override_config(r, overrides)
    r.assert_valid()

    return r


def override_config(config, overrides):

    flags = type(config).flags()
    default_config = DefaultConfig()

    if overrides is not None:

        print()
        if overrides is not None:
            for k, v in list(overrides.items()):
                assert k in flags, (k, flags)
                print(k, v)
                if v is None:
                    print('not overriding with None:', k)
                else:
                    v0 = config.__dict__[k]
                    assert v0 is None or isinstance(v, type(v0)), (k, v, type(v), v0, type(v0))
                    print('get_config override', k, '=', v0, '->', v)
                    config.__setattr__(k, v)
        print()

    print(type(config))
    for k in flags:
        if k not in config.__dict__:
            config.__dict__[k] = default_config.__dict__[k]
            print('backwards compatible default:')
        print('\t%s = %s' % (str(k).ljust(30), config.__dict__[k]))

    config.assert_valid()

    return config


classes = inspect.getmembers(sys.modules[__name__], inspect.isclass)


def members(x):
    return [k for k, v in inspect.getmembers(x, lambda a: not(inspect.isroutine(a))) if not(k.startswith('__') and k.endswith('__'))]


def non_defaults(x, x_default):
    return set(members(x)).difference(set(members(x_default)))


for s, cls in classes:
    nd = non_defaults(cls(), DefaultConfig())
    assert len(nd) == 0, (cls, nd)
