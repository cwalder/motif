# dataset loading etc; overly complex as it was adapted from a more general setup

import collections, itertools, os
import gridtools.helpers as gh
import numpy as np

import motif.model.reader as dmr
from motif import helpers


class DataSet(dmr.DataSet):

    def initialise(self):

        super(DataSet, self).initialise(build_mes=False)
        self.i2midi, self.midi2i, self.midi_hi = helpers.i2val_val2i_len(self.support['midi_set'])

    def print_lens(self, split, lens):
        v = tuple([split, gh.pretty_number(len(lens))] + list(gh.pretty_number(_) for _ in [f(lens) for f in (min, np.median, max)]))
        print('raw_data: split = %s sentences = %s min / median / max len = %s / %s / %s' % v)

    def raw_data(self, split, shuffle):

        if split not in self.raw_data_cache:

            events_list = []
            tuples_list = self.load_smd_iterator(split)

            for i_t, t in enumerate(tuples_list):

                file, t0, t1, midi, part = zip(*sorted(t))
                part2midis = dict()
                for this_midi, this_part in zip(midi, part):
                    part2midis.setdefault(this_part, []).append(this_midi)

                for this_part in sorted(part2midis.keys()):
                    events_list.append(tuple(map(self.midi2i.__getitem__, part2midis[this_part])))

            lens = tuple(map(len, events_list))
            self.print_lens(split, lens)

            if (self.configs[split].data_subset_proportion_pieces_shortest is not None) and (self.configs[split].data_subset_proportion_pieces_shortest != 1):
                assert self.configs[split].data_subset_proportion_pieces_shortest < 1
                n_subset_shortest = int(np.ceil(self.configs[split].data_subset_proportion_pieces_shortest * len(lens)))
                if n_subset_shortest < len(lens):
                    print('n_subset_shortest %i -> %i' % (len(lens), n_subset_shortest))
                    indices = np.argsort(lens)[:n_subset_shortest]
                    events_list = gh.sub_list(events_list, indices)
                    lens = tuple(map(len, events_list))
                    self.print_lens(split, lens)

            self.raw_data_cache[split] = tuple(events_list)

        rval = self.raw_data_cache[split]

        if shuffle:
            rval = helpers.randomly(rval)

        return rval

    def embedding(self):

        return np.array(self.i2midi, dtype=float)

    def n_data(self, split):

        return len(self.raw_data(split, False))

    def dump(self, path):

        for split in DataSet.SPLITS:

            d = self.raw_data(split, False)

            with open('%s/%s.csv' % (path, split), 'w') as fp:
                lines = [','.join(map(str, _)) + '\n' for _ in d]
                fp.writelines(map(str, lines))

            gh.dump(d, '%s/%s.pkl' % (path, split))


class CharDataSet(dmr.CharDataSet):

    def initialise(self):

        super(CharDataSet, self).initialise(build_mes=False)
        self._raw = None

    def print_lens(self, split, lens):
        v = tuple([split, gh.pretty_number(len(lens))] + list(gh.pretty_number(_) for _ in [f(lens) for f in (min, np.median, max)]))
        print('raw_data: split = %s sentences = %s min / median / max len = %s / %s / %s' % v)

    def raw_data(self, split, shuffle):

        if split not in self.raw_data_cache:

            if self._raw is None:
                with open(os.path.expanduser(self.dataset_name), 'r') as fp:
                    self._raw = fp.read().splitlines()
                if self.delimiter is not None:
                    self._raw = tuple(_.strip(self.delimiter) for _ in self._raw)
                    self._raw = filter(len, self._raw)
                    self._raw = tuple(tuple(_.split(self.delimiter)) for _ in self._raw)
                if self.configs[split].data_subset_ratio_pieces is not None and self.configs[split].data_subset_ratio_pieces != 1:
                    assert self.configs[split].data_subset_ratio_pieces > 1
                    print('self.configs[split].data_subset_ratio_pieces', self.configs[split].data_subset_ratio_pieces)
                    self._raw = tuple(helpers.subsample_iterator(self._raw, self.configs[split].data_subset_ratio_pieces))
                assert all(_ > 0 for _ in map(len, self._raw))
                self.support = set(itertools.chain(*self._raw))
                self.i2midi, self.midi2i, self.midi_hi = helpers.i2val_val2i_len(self.support, eos=dmr.CharDataSet.EOS)
                self.eos_index = self.midi2i[dmr.CharDataSet.EOS]
                print('raw_data: %i symbols' % self.midi_hi)

            inds = 0, int(0.7 * len(self._raw)), int(0.85 * len(self._raw)), None
            ranges = dict(
                train=(inds[0], inds[1]),
                valid=(inds[1], inds[2]),
                test=(inds[2], inds[3])
            )

            r = self._raw[slice(*ranges[split])]
            events_list = tuple(tuple(list(map(self.midi2i.__getitem__, _))+[self.eos_index]) for _ in r)

            lens = tuple(map(len, events_list))
            self.print_lens(split, lens)

            if (self.configs[split].data_subset_proportion_pieces_shortest is not None) and (self.configs[split].data_subset_proportion_pieces_shortest != 1):
                assert self.configs[split].data_subset_proportion_pieces_shortest < 1
                n_subset_shortest = int(np.ceil(self.configs[split].data_subset_proportion_pieces_shortest * len(lens)))
                if n_subset_shortest < len(lens):
                    print('n_subset_shortest %i -> %i' % (len(lens), n_subset_shortest))
                    indices = np.argsort(lens)[:n_subset_shortest]
                    events_list = gh.sub_list(events_list, indices)
                    lens = tuple(map(len, events_list))
                    self.print_lens(split, lens)

            self.raw_data_cache[split] = tuple(events_list)

        rval = self.raw_data_cache[split]

        if False:
            print(split)
            print(self.i2midi)
            for r in rval[:10]:
                print(r)
                print(tuple(map(self.i2midi.__getitem__, r)))

        if shuffle:
            return helpers.randomly(rval)
        else:
            return rval

    def embedding(self):

        return np.array(self.i2midi, dtype=float)

    def n_data(self, split):

        return len(self.raw_data(split, False))

    def dump(self, path):

        for split in DataSet.SPLITS:

            d = self.raw_data(split, False)

            with open('%s/%s.csv' % (path, split), 'w') as fp:
                lines = [','.join(map(str, _)) + '\n' for _ in d]
                fp.writelines(map(str, lines))

            gh.dump(d, '%s/%s.pkl' % (path, split))


def make_dataset(dataset_str, config):

    if '_char.txt' in dataset_str:
        ds = CharDataSet(dataset_str, config)
        ds.initialise()
        ds.raw_data(CharDataSet.SPLITS[0], shuffle=False)
        return ds
    if '_word.txt' in dataset_str:
        ds = CharDataSet(dataset_str, config, delimiter=' ')
        ds.initialise()
        ds.raw_data(CharDataSet.SPLITS[0], shuffle=False)
        return ds
    else:
        return DataSet(dataset_str, config)
