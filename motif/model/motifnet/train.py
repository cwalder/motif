'''
main training program. example command line usage:

python train.py --model "'lstm'"
python train.py --model "'motifnet'"
python train.py --model "'motifnetpluslstm'"
'''

import copy
import itertools

import gridtools.helpers as gh
import gridtools.paths as gp

import motif.model.motifnet.config as dmc
import motif.model.motifnet.model as dmmmm
import motif.model.motifnet.reader as dmr
import motif.model.motifnet.helpers as dmmmh
from motif import helpers

_result_path = '%s/result.pkl'
_vm_save_path = '%s/vm.pkl'
_model_save_path = '%s/model.pkl'
_sample_path = '%s/sample/'

_model = 'lstm'
_model = 'motifnet'
_model = 'motifnetpluslstm'
_overrides = dict(
    n_tree_expand=16,
    num_threads=None,
)
_quick = True
_quick_overrides = dict(
    data_subset_proportion_pieces_shortest = 0.05,
    max_epoch = 2,
    hidden_size=4,
    embedding_size=4,
    l_max=2,
)
_dataset_str = '~/output/cleaner/2018_07_29_14_59_03_460788_276111914/not'
# _dataset_str = '/tmp/test_char.txt'
# _dataset_str = '/tmp/test_word.txt'
_dataset_str = '/tmp/ptb.train_word.txt'
_subpath = ''
_basepath = None
_path = None
_save_model = False


@gh.dumpArgs()
def raw_main(
        model=_model,
        quick=_quick,
        overrides=_overrides,
        dataset_str=_dataset_str,
        subpath=_subpath,
        basepath=_basepath,
        save_model=_save_model,
):

    path = gp.output('motifnet' if (subpath is None or subpath == '') else subpath, basepath=basepath, create=False)
    gh.dumpArgsToDirectory(path)

    if quick:
        overrides.update(_quick_overrides)

    run_path = gp.output('run', basepath=path)

    tee = gh.Tee('%s/output.txt' % run_path, compress=False)

    config = dmc.get_config(model, overrides=overrides)
    eval_config = copy.deepcopy(config)
    eval_config.batch_size = eval_config.num_steps = 1

    dataset = dmr.make_dataset(dataset_str, config)

    model = dmmmm.Model.get_model(config=config, midi_hi=dataset.midi_hi)  # , embedding=dataset.embedding())
    model.make_optimizer()

    vm = helpers.ValidationMonitor()

    for __i_epoch in itertools.count():

        train_cost = model.run_epoch(dataset, 'train', shuffle=__i_epoch != 0)

        valid_cost = model.run_epoch(dataset, 'valid', shuffle=__i_epoch != 0)

        vm.update(train_cost, valid_cost)
        gh.dump(vm, _vm_save_path % path)

        if vm.improved('valid'):

            if save_model:
                model.save(_model_save_path % path)

            test_cost = model.run_epoch(dataset, 'test', shuffle=__i_epoch != 0)

            result = dict(path=gp.unexpand_home(path), partial=True, vm=vm, test_cost=test_cost)
            gh.dump(result, _result_path % path)

        if vm.train_strikes >= config.max_train_strikes:
            print('config.max_train_strikes exceeded')
            break

        if vm.strikes >= config.max_strikes:
            print('config.max_strikes exceeded')
            break

        if len(vm) >= config.max_epoch:
            break

    vm.print()

    print('final test_cost', test_cost)

    result['partial'] = False
    gh.dump(result, _result_path % path)

    tee.to_file()

    return result


main = gh.auto_parser_inspector(allow_missing=True)(raw_main)

if __name__ == '__main__':

    main()
