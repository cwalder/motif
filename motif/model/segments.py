# abstract data structures for midi

import itertools
import collections
import numpy as np
import smd.data
import smd.io
from motif import constants, helpers


class Segments(object):

    EOS_S = 0

    def __init__(self, t0s, t1s, midis, parts, sort=True, check=True):
        assert len(set(map(len, (t0s, t1s, midis, parts)))) == 1
        if sort and len(t0s):
            t0s, t1s, parts, midis = zip(*sorted(tuple(zip(t0s, t1s, parts, midis))))
        self.sort = sort
        self.t0s = t0s
        self.t1s = t1s
        self.midis = midis
        self.parts = parts
        self.d = None
        if check:
            self.self_check()

    @classmethod
    def from_smd_tuples(cls, t, columns=('t0', 't1', 'midi', 'part'), check=True):

        tz = tuple(zip(*t))
        tt = [tz[smd.data.column2index[c]] for c in columns]

        return Segments(*tt, check=check)

    @property
    def s_set(self):
        # does not include constants.EOS, always includes 0
        t_all = np.array(tuple(set(itertools.chain(self.t0s, self.t1s))))
        t_all.sort()
        rval = set(np.diff(t_all))
        rval.add(t_all[0])
        rval.add(0)
        if self.t0s[0] != 0:
            rval.add(self.t0s[0])
        return rval

    def self_check(self):
        assert len(self.t0s) == len(self.t1s) == len(self.midis) == len(self.parts)
        assert len(self) == 0 or min(self.t0s) >= 0, (min(self.t0s), self.t0s)
        assert np.all(np.array(self.t1s) > np.array(self.t0s))
        if not self.sort:
            tup_prev = None
            for tup in zip(self.t0s, self.t1s, self.parts, self.midis):
                assert tup_prev is None or tup >= tup_prev
                tup_prev = tup

    def build_dict(self):
        self.d = collections.OrderedDict()
        for t, midi, part, delta in sorted(itertools.chain(zip(self.t0s, self.midis, self.parts, itertools.repeat(constants.ONSET)), zip(self.t1s, self.midis, self.parts, itertools.repeat(constants.OFFSET)))):
            self.d.setdefault(t, collections.OrderedDict()).setdefault(midi, collections.OrderedDict()).setdefault(part, []).append(delta)

    def __event_iterator(self):

        if self.d is None:
            self.build_dict()

        off_on = [constants.OFFSET, constants.ONSET]

        midi_hi = max(self.midis) + 1
        part_hi = max(self.parts) + 1

        u = np.zeros((midi_hi, part_hi), dtype=int)
        t_prev = 0
        for t, dict_t in self.d.items():
            midi_part_delta_list = []
            for midi, dict_t_midi in dict_t.items():
                for part, deltas in dict_t_midi.items():
                    if len(deltas) == 0:
                        continue
                    elif len(deltas) == 1:
                        delta = deltas[0]
                    else:
                        if deltas != off_on:
                            raise MidiDataException('t %i midi %i part %i deltas %s' % (t, midi, part, str(deltas)))
                        delta = constants.ONSET
                        u[midi, part] = 0
                    midi_part_delta_list.append((midi, part, delta))
                    if delta == constants.ONSET:
                        if u[midi, part]:
                            raise MidiDataException(u[midi, part], midi, part)
                    u[midi, part] = min(1, max(0, u[midi, part] + delta))
            yield t - t_prev, midi_part_delta_list
            t_prev = t

    def event_iterator(self):

        if self.t0s[0] != 0:
            yield self.t0s[0], []

        for s, midi_part_delta_list in helpers.stagger(self.__event_iterator(), final_a=Segments.EOS_S):
            yield s, midi_part_delta_list

    def plot(self):

        from gridtools.plotting import plt
        import gridtools.helpers as gh

        colors = helpers.get_color_range(max(self.parts)+1)

        pb = gh.ProgressBar(len(self), 10, 10, label='Segments.plot')
        for t0, t1, midi, part in zip(self.t0s, self.t1s, self.midis, self.parts):
            pb()
            plt.plot([t0, t1], [midi, midi], color=colors[part])
            plt.plot([t0], [midi], marker='.', markersize=4, color=colors[part])
            plt.plot([t0], [midi], marker='|', markersize=4, color=colors[part])

    @classmethod
    def is_eos_event(cls, event):
        s, _ = event
        return s == Segments.EOS_S

    def write_midi(self, filename):

        return smd.io.write_midi(self.t0s, self.t1s, self.midis, self.parts, filename)

    def __len__(self):
        return len(self.t0s)

    def __eq__(self, other):
        self.self_check()
        other.self_check()
        if len(self) != len(other): return False
        if tuple(self.t0s) != tuple(other.t0s): return False
        if tuple(self.t1s) != tuple(other.t1s): return False
        if tuple(self.midis) != tuple(other.midis): return False
        if tuple(self.parts) != tuple(other.parts): return False
        return True

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        s = super(Segments, self).__str__()
        s += '\nt0\tt1\tmid\tprt\n' + '\n'.join(['\t'.join(map(str, _)) for _ in zip(self.t0s, self.t1s, self.midis, self.parts)])
        return s


class TimeDeltas(object):

    def __init__(self, times):

        assert times[0] == 0
        assert all(0 <= time and isinstance(time, int) for time in times)
        self.times = tuple(times)
        assert self.times == tuple(sorted(times))


class MidiDataException(Exception):
    pass
