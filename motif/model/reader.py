# data reader

import itertools

import gridtools.paths as gp
import smd.data

import motif.data.loader as dmdl
import motif.model.segments as dms
from motif import helpers, constants


def midi_modulo(x, n):
    return tuple((file_index, t0, t1, midi % n, part) for file_index, t0, t1, midi, part in x)


class DataSet(object):

    SPLITS = dmdl.split_names

    def __init__(self, dataset_name, configs, splits=None, mes=None):

        self.dataset_name = dataset_name
        if splits is None:
            splits = DataSet.SPLITS
        self.splits = splits
        if isinstance(configs, dict):
            self.configs = configs
        else:
            self.configs = {split: configs for split in self.splits}
        self.raw_data_cache = dict()
        self.mes = mes

        self.initialise()

    def load_smd_iterator(self, split):
        print('load_smd_iterator', split)
        if 'cleaner' in self.dataset_name or 'toy' in self.dataset_name:
            rval = dmdl.load_tuples(gp.expanduser(self.dataset_name), split)
        elif isinstance(self.dataset_name, dict):
            fns = self.dataset_name[split]
            assert all(_.endswith('.mid') for _ in fns)
            import wpack.music.functions as mf
            assert smd.data.columns == ('file_index', 't0', 't1', 'midi', 'part')
            column2key = dict(file_index='file_index', t0='t0s', t1='t1s', midi='midis', part='parts')

            parsed = [mf.parse_midi_via_csv(fn) for fn in fns]
            rval = []
            for file_index, p in enumerate(parsed):
                p['file_index'] = itertools.repeat(file_index)
                rval.append(tuple(zip(*[p[column2key[c]] for c in smd.data.columns])))
                assert p['stats']['resolution'] == constants.RESOLUTION

        else:
            rval = (itertools.chain(*(smd.data.load_tuples(_, split) for _ in self.dataset_name.split('_'))))
        if self.configs[split] is not None:
            if self.configs[split].allowed_parts is not None:
                print('self.configs[split].allowed_parts', self.configs[split].allowed_parts)
                i = smd.data.column2index['part']
                nparts = lambda this_tuple: 1 + max(_[i] for _ in this_tuple)
                rval = (_ for _ in rval if nparts(_) == self.configs[split].allowed_parts)
            if self.configs[split].data_subset_ratio_pieces is not None and self.configs[split].data_subset_ratio_pieces != 1:
                assert self.configs[split].data_subset_ratio_pieces > 1
                print('self.configs[split].data_subset_ratio_pieces', self.configs[split].data_subset_ratio_pieces)
                rval = helpers.subsample_iterator(rval, self.configs[split].data_subset_ratio_pieces)
            if self.configs[split].data_subset_ratio_events is not None and self.configs[split].data_subset_ratio_events != 1:
                assert self.configs[split].data_subset_ratio_events > 1
                print('self.configs[split].data_subset_ratio_events', self.configs[split].data_subset_ratio_events)
                rval = (_[::self.configs[split].data_subset_ratio_events] for _ in rval)
            if self.configs[split].data_modulo_midi is not None:
                print('self.configs[split].data_modulo_midi', self.configs[split].data_modulo_midi)
                rval = (midi_modulo(t, self.configs[split].data_modulo_midi) for t in rval)

        return rval

    def initialise(self, build_mes=True):

        s_set, midi_set, part_set = {0}, set(), set()

        for split in self.splits:
            tuples_list = self.load_smd_iterator(split)
            for i_t, t in enumerate(tuples_list):
                segments = dms.Segments.from_smd_tuples(t)
                if self.configs[split] is not None and self.configs[split].transpose_train and split == 'train':
                    this_midis = list(itertools.chain(*[[_ + offset for _ in segments.midis] for offset in set(self.configs[split].transposes)]))
                else:
                    this_midis = segments.midis
                assert min(this_midis) >= 0, this_midis
                assert max(this_midis) < 128
                s_set.update(segments.s_set)
                midi_set.update(this_midis)
                part_set.update(segments.parts)

        if not build_mes:

            self.support = dict(s_set=s_set, midi_set=midi_set, part_set=part_set)

        else:

            raise

    def clean(self):
        self.raw_data_cache = None


class CharDataSet(object):

    SPLITS = dmdl.split_names
    EOS = '<<eos>>'

    def __init__(self, dataset_name, configs, splits=None, mes=None, delimiter=None):

        self.delimiter = delimiter
        self.dataset_name = dataset_name
        if splits is None:
            splits = DataSet.SPLITS
        self.splits = splits
        if isinstance(configs, dict):
            self.configs = configs
        else:
            self.configs = {split: configs for split in self.splits}
        self.raw_data_cache = dict()
        self.initialise()

    def initialise(self, build_mes=True):
        pass

    def clean(self):
        self.raw_data_cache = None
