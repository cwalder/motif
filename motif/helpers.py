from gridtools.plotting import plt
import math
import random
import time
import itertools
import numpy as np
import contextlib
import collections
from pylab import get_cmap
import pandas as pd
import inspect
import gridtools.helpers as gh
import scipy.misc
import scipy.stats
from motif import constants

try:
    import tensorflow as tf
except:
    pass


class NonWrappingTuple(tuple):

    def __getitem__(self, item):
        if not 0 <= item < len(self):
            raise KeyError(item)
        return tuple.__getitem__(self, item)


def i2val_val2i_len(vals, eos=None):
    assert isinstance(vals, set)
    if eos is not None:
        assert eos not in vals
        vals.add(eos)
    i2val = NonWrappingTuple(sorted(vals))
    val2i = {val: i for i, val in enumerate(i2val)}
    return i2val, val2i, len(i2val)


def len_checking_zip(*args):
    assert len(set(map(len, args))) == 1
    return zip(*args)


def list_dicts(d, keys=None):
    return {k: [_[k] for _ in d] for k in (d[0].keys() if keys is None else keys)}


def stagger(pairs, final_a):
    first = True
    for a, b in pairs:
        if not first:
            yield a, b_prev
        b_prev = b
        first = False
    if not first:
        yield final_a, b


# train / validation helper
class ValidationMonitor(object):

    def __init__(self):

        self.strikes = 0
        self.train_strikes = 0
        self.previous_strikes = 0
        self.train_costs = []
        self.valid_costs = []
        self.strikes_list = []
        self.train_strikes_list = []
        self.previous_strikes_list = []
        self.t_list = []
        self.tprev = time.time()
        self.auxiliary = []

    def update(self, train_cost, valid_cost, **auxiliary):

        self.train_costs.append(train_cost)
        self.valid_costs.append(valid_cost)
        self.auxiliary.append(auxiliary)

        if self.improved('valid'):
            self.strikes = 0

        if self.improved('train') and not self.improved('valid'):
            self.strikes += 1
            self.previous_strikes += 1

        if self.improved('train'):
            self.train_strikes = 0
        else:
            self.train_strikes += 1

        print('iter {:3d} strikes {:2d} : {:2d} : {:2d} : train = {:8.3f} : {:8.3f} percent ; valid = {:10.3f} : {:10.3f} percent {:s}'.format(len(self), self.strikes, self.previous_strikes, self.train_strikes, self.best('train'), self.pct_improved('train'), self.best('valid'), self.pct_improved('valid'), gh.pretty_duration(time.time()-self.tprev)))

        t = time.time()
        self.strikes_list.append(self.strikes)
        self.train_strikes_list.append(self.train_strikes)
        self.previous_strikes_list.append(self.previous_strikes)
        self.t_list.append(t-self.tprev)
        self.tprev = t

    def best(self, s):
        if len(self) == 0:
            return np.nan
        if s == 'valid':
            return min(self.valid_costs, default=np.inf)
        elif s == 'train':
            return min(self.train_costs, default=np.inf)
        else:
            raise Exception(s)

    def improved(self, s):
        if s == 'valid':
            return min(self.valid_costs[:-1], default=np.inf) > self.valid_costs[-1]
        elif s == 'train':
            return min(self.train_costs[:-1], default=np.inf) > self.train_costs[-1]
        else:
            raise Exception(s)

    def pct_improved(self, s):
        if len(self) < 2:
            return np.nan
        if s == 'valid':
            return 100 * (min(self.valid_costs[:-1]) - self.valid_costs[-1]) / self.best('valid')
        if s == 'train':
            return 100 * (min(self.train_costs[:-1]) - self.train_costs[-1]) / self.best('train')
        else:
            raise Exception(s)

    def to_df(self):
        assert 1 == len(set(map(len, self.auxiliary)))
        if len(self.auxiliary[0]):
            keys, vals = map(list, zip(*list_dicts(self.auxiliary).items()))
            assert 1 == len(set(map(len, vals))), vals
        else:
            keys, vals = [], []
        assert 1 == len(set(map(len, [self.train_costs, self.valid_costs, self.strikes_list, self.previous_strikes_list, self.train_strikes_list, self.t_list, self.auxiliary] + vals)))
        return pd.DataFrame(
            data=list(zip(self.train_costs, self.valid_costs, self.strikes_list, self.previous_strikes_list, self.train_strikes_list, self.t_list, *vals)),
            columns=['train', 'valid', 'strikes', 'previous_strikes', 'train_strikes', 'time'] + list(keys)
        )

    def print(self):
        with pd.option_context('display.width', 9999, 'display.max_columns', 999):
            print(self.to_df())

    def __len__(self):
        return len(self.train_costs)


def subsample_iterator(x, n):
    return (xx for i, xx in enumerate(x) if not i % n)


def randomly(i):
    ind = np.arange(len(i))
    np.random.shuffle(ind)
    for this_ind in ind:
        yield i[this_ind]
