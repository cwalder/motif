# global constants

from smd import data
import numpy as np

max_parts_dict = {'jsb': {'test': 0, 'train': 0, 'valid': 0}, 'pmd': {'test': 4, 'train': 5, 'valid': 5}, 'jbm': {'test': 3, 'train': 3, 'valid': 3}, 'not': {'test': 1, 'train': 2, 'valid': 1}, 'cma': {'test': 33, 'train': 45, 'valid': 39}, 'mus': {'test': 17, 'train': 26, 'valid': 13}}

MIDI_HI = 128
PART_HI = 45 + 1
RESOLUTION = data.resolution
dataset_names = ['jbm', 'pmd', 'not', 'mus', 'cma']
OFFSET, HOLD, ONSET = -1, 0, 1
OFFSET_INDEX, HOLD_INDEX, ONSET_INDEX = 0, 1, 2
N_OFFSETS = 3
INDEX_TO_DELTA = OFFSET, HOLD, ONSET
QUICK_TESTS = True

NP_FLOAT = np.float32
NP_INT = np.int16

_pb_freq = 100
_pb_tfreq = 30
