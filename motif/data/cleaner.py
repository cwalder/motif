# music specific preprocessing tricks for what is essentially raw midi data

import numpy as np
import copy
import itertools
from gridtools.plotting import plt
import gridtools.paths as gp
import gridtools.helpers as gh
import smd.data
import smd.io
import motif.helpers as dh
import motif.model.segments as dms
import scipy.stats.mstats


assert smd.data.columns == ('file_index', 't0', 't1', 'midi', 'part')


def get_summary(t):
    n = len(t)
    tz = tuple(zip(*t))
    part, midi = (np.array(tz[smd.data.column2index[k]], dtype=int) for k in ('part', 'midi'))
    n_parts = max(part) + 1
    midi_lo = min(midi)
    midi_hi = max(midi) + 1
    inds = tuple(part == i for i in range(n_parts))
    median_midi = np.array([np.median(midi[ind]) for ind in inds])
    part_count = np.array(tuple(map(sum, inds)))
    # assert np.all(median_midi[1:] >= median_midi[:-1])
    return dict(n=n, n_parts=n_parts, midi_lo=midi_lo, midi_hi=midi_hi, median_midi=median_midi, part_count=part_count)


@gh.memodict
def _inds(n):
    return np.array(tuple(itertools.combinations(list(range(n[0])), n[1])), dtype=int)


def inds(n_target, n):
    return _inds((n_target, n))


def reassign_fewer_parts(median_midis, target_median_midis):
    i = inds(len(target_median_midis), len(median_midis))
    errors = np.sum(np.abs(target_median_midis[i]-median_midis), axis=1)
    return i[np.argmin(errors)]


def reassign_more_parts(median_midis, part_counts, target_median_midis, merge_penalty):
    counter = np.zeros(len(target_median_midis), dtype=int)
    inds = [0] * len(part_counts)
    for i in reversed(np.argsort(part_counts)):
        error = counter * merge_penalty + np.abs(median_midis[i] - target_median_midis)
        ibest = np.argmin(error)
        inds[i] = ibest
        counter[ibest] += 1
    return tuple(inds)


def plot_tuples(t, colors):
    pb = gh.ProgressBar(len(t), 10, 10, label='plot_tuples')
    first = np.ones(len(colors), dtype=bool)
    for (file_index, t0, t1, midi, part) in sorted(t, key=lambda x: x[-1]):
        pb()
        plt.plot([t0 / smd.data.resolution, t1 / smd.data.resolution], [midi+part/len(colors)/2, midi+part/len(colors)/2], color=colors[part], label=part if first[part] else None)
        first[part] = False
    plt.legend(loc='lower left')
    plt.axis([None, None, 0, 128])


def reassign_tuple_parts_ind(t, i):
    return tuple((file_index, t0, t1, midi, i[part]) for (file_index, t0, t1, midi, part) in t)


def reassign_tuple_parts(t, target_median_midis, merge_penalty):

    summary = get_summary(t)

    if summary['n_parts'] < len(target_median_midis):
        i = reassign_fewer_parts(summary['median_midi'], target_median_midis)
    elif summary['n_parts'] > len(target_median_midis):
        i = reassign_more_parts(summary['median_midi'], summary['part_count'], target_median_midis, merge_penalty)
    else:
        return t

    tr = reassign_tuple_parts_ind(t, i)

    return tr


def invert_order(i):
    ii = np.zeros(len(i), dtype=int)
    ii[i] = np.arange(len(i), dtype=int)
    return ii


def reorder_parts(t):

    sc = get_summary(t)
    sc['median_midi'][np.isnan(sc['median_midi'])] = 999
    i = np.argsort(sc['median_midi'])
    tcr = reassign_tuple_parts_ind(t, invert_order(i))

    return tcr


def clean_unison_parts(t):

    s = get_summary(t)
    part_count = s['part_count']
    partless_events = set()

    rval = []
    for event in sorted(t, key=lambda _: part_count[_[-1]], reverse=True):
        (file_index, t0, t1, midi, part) = event
        partless_event = (t0, t1, midi)
        if partless_event not in partless_events:
            partless_events.add(partless_event)
            rval.append(event)

    npre = len(t)
    npost = len(rval)
    print('clean_unison_parts cleaned %6.2f%% of %i' % (100 * float(npre - npost) / npre, npost))

    return rval


def clean_short_parts(t, min_part_events):

    s = get_summary(t)
    part_count = s['part_count']

    rval = []
    for event in sorted(t, key=lambda _: part_count[_[-1]], reverse=True):
        (file_index, t0, t1, midi, part) = event
        if part_count[part] >= min_part_events:
            rval.append(event)

    npre = len(t)
    npost = len(rval)
    print('clean_short_parts cleaned %6.2f%% of %i' % (100 * float(npre - npost) / npre, npost))

    return rval


def _clean_overlaps(t0t1):
    t1prev = -np.inf
    for t0, t1 in t0t1:
        if t0 >= t1prev:
            yield (t0, t1)
            t1prev = t1


def clean_overlaps(t):

    d = dict()
    for file_index, t0, t1, midi, part in sorted(t):
        d.setdefault((midi, part), []).append((t0, t1))
    rval = []
    for (midi, part), t0t1 in d.items():
        for t0, t1 in _clean_overlaps(t0t1):
            rval.append((file_index, t0, t1, midi, part))

    npre = len(t)
    npost = len(rval)
    print('clean_overlaps cleaned %6.2f%% of %i' % (100 * float(npre - npost) / npre, npost))

    return tuple(rval)


def clean_legatize(t, dtmax):

    file_index, t0, t1, midi, part_indices = list(zip(*sorted(t)))

    setpre = set(itertools.chain(t0, t1))
    npre = len(setpre)

    t0, t1 = _legatize(t0, t1, dtmax)

    setpost = set(itertools.chain(t0, t1))
    npost = len(setpost)

    if npre != npost:
        print('clean_legatize cleaned %.2f%% of %i' % (100 * float(npre - npost) / npre, npost))

    return tuple(zip(file_index, t0, t1, midi, part_indices))


def _legatize(t0s, t1s, dtmax):
    assert len(t0s) == len(t1s)
    assert tuple(t0s) == tuple(sorted(t0s)), (list(zip(t0s, sorted(t0s))))
    assert all(np.array(t0s) < np.array(t1s))
    assert dtmax > 0

    tt = sorted(set(itertools.chain(t0s, t1s)), reverse=True)
    t2knot = dict()
    tprev = tt[0]
    for t in tt:
        if (tprev - t) > dtmax:
            tprev = t
        t2knot[t] = tprev
    assert all(k<=v for k, v in list(t2knot.items()))
    tcat = list(itertools.chain(t0s, t1s))
    n = len(t0s)
    i_other = list(range(n,n+n)) + list(range(n))
    isort = list(reversed(np.argsort(tcat)))
    for j in range(1,n+n):
        jj = isort[j]
        jjprev = isort[j-1]
        old = tcat[jj]
        new = t2knot[old]
        if old != new:
            if new <= tcat[jjprev]:
                if tcat[i_other[jj]] != new:
                    tcat[jj] = new
    t0sl = tcat[:n]
    t1sl = tcat[n:(n+n)]

    return t0sl, t1sl


def plot_statistics(summaries_dict, label):

    summaries_dict = copy.copy(summaries_dict)

    part_count = summaries_dict.pop('part_count')
    median_midi = summaries_dict.pop('median_midi')
    median_midi_i = lambda x: [_[x] for _ in median_midi if len(_) >= (x+1) and not np.isnan(_[x])]
    part_count_i = lambda x: [_[x] for _ in part_count if len(_) >= (x+1)]
    max_n_parts = max(summaries_dict['n_parts'])

    plt.figure()
    ax = None
    for i in range(max_n_parts):
        ax = plt.subplot(max_n_parts, 1, i+1, sharex=ax)
        plt.hist(median_midi_i(i), bins=30, normed=True, label=str(i))
        plt.axis('off')
        plt.yticks([])
    plt.suptitle('median_midi ' + label)

    plt.figure()
    ax = None
    for i in range(max_n_parts):
        ax = plt.subplot(max_n_parts, 1, i+1, sharex=ax)
        plt.hist(part_count_i(i), bins=30, normed=True, label=str(i))
        plt.axis('off')
        plt.yticks([])
    plt.suptitle('part count ' + label)


def split_map(f, tts, mms, label=None):
    g = gh.progress_barred(split_len(tts), 100, 5, label=str(f) if label is None else label)(f)
    ttmm = tuple(tuple([(t, m) for t, m in zip(map(g, tt), mm) if t is not None]) for tt, mm in zip(tts, mms))
    tts = tuple(tuple(t for t, m in tm) for tm in ttmm)
    mms = tuple(tuple(m for t, m in tm) for tm in ttmm)
    return tts, mms


def split_len(tt):
    return sum(map(len, tt))


@gh.dumpArgs()
def raw_main(
        # dataset_str='cma_mus_jbm_pmd_not',
        # target_n_parts=6,
        split_str='train_test_valid',
        # dataset_str=['jbm', 'mus', 'pmd', 'not'],
        dataset_str=['not'],
        target_n_parts=16,
        min_events=100,
        min_part_events=20,
        merge_penalty=0,
        legatize_dtmax_quarternotes=1.0/96,
        do_plot=False,
        do_plot_tuples=False,
        path=None,
        dumpargs=False,
):

    if path is None:
        path = gp.output('cleaner', create=True)

    if isinstance(dataset_str, list):
        for s in dataset_str:
            p = gp.safe_mkdir('%s/%s' % (path, s))
            raw_main(dataset_str=s, split_str=split_str, target_n_parts=target_n_parts, min_events=min_events, min_part_events=min_part_events, merge_penalty=merge_penalty, legatize_dtmax_quarternotes=legatize_dtmax_quarternotes, do_plot=do_plot, do_plot_tuples=do_plot_tuples, path=p, dumpargs=dumpargs)
        return

    if dumpargs:
        gh.dumpArgsToDirectory(path)

    tts = tuple(tuple(itertools.chain(*(smd.data.load_tuples(d, s) for d in dataset_str.split('_')))) for s in split_str.split('_'))
    mms = tuple(tuple(itertools.chain(*([tempo for file_index, tempo in sorted(smd.data.load_tempos(d, s).items())] for d in dataset_str.split('_')))) for s in split_str.split('_'))

    print('lens', split_len(tts), list(map(len, tts)))

    if legatize_dtmax_quarternotes is not None:
        tts, mms = split_map(lambda _: clean_legatize(_, int(legatize_dtmax_quarternotes * smd.data.resolution)), tts, mms, 'legatize')

    tts, mms = split_map(clean_unison_parts, tts, mms)

    tts, mms = split_map(lambda _: clean_short_parts(_, min_part_events), tts, mms, 'clean_short_parts')

    tts, mms = split_map(lambda _: _ if len(_) >= min_events else None, tts, mms)

    tts, mms = split_map(reorder_parts, tts, mms)

    summaries, _ = split_map(get_summary, tts, mms)
    summaries_dict = dh.list_dicts(list(itertools.chain(*summaries)))

    if do_plot:
        plot_statistics(summaries_dict, 'raw')

    median_midi = summaries_dict.pop('median_midi')
    prob = np.linspace(0, 1, target_n_parts*2+1, endpoint=True)[1::2]
    target_median_midis = scipy.stats.mstats.mquantiles(list(itertools.chain(*median_midi)), prob=prob)

    print('target_median_midis', target_median_midis)

    tts, mms = split_map(lambda _: reassign_tuple_parts(_, target_median_midis, merge_penalty), tts, mms, 'reassign_tuple_parts')

    tts, mms = split_map(clean_overlaps, tts, mms)

    split_map(lambda _: list(dms.Segments.from_smd_tuples(_).event_iterator()), tts, mms, 'event_iterator')

    print('lens', split_len(tts), list(map(len, tts)))

    for split, tt, mm in zip(split_str.split('_'), tts, mms):

        filename = '%s/cleaned_%s.csv' % (path, split)
        print(filename)
        with open(filename, 'w') as fp:
            fp.write('file_index,t0,t1,midi,part\n')
            for ifile, t in enumerate(tt):
                for (file_index, t0, t1, midi, part) in sorted(t):
                    fp.write('%i,%i,%i,%i,%i\n' % (ifile, t0, t1, midi, part))

        filename = '%s/cleaned_%s_tempo.csv' % (path, split)
        print(filename)
        with open(filename, 'w') as fp:
            fp.write('file_index,tempo\n')
            for ifile, m in enumerate(mm):
                fp.write('%i,%i\n' % (ifile, m))

        filename = '%s/resolution.txt' % path
        print(filename)
        with open(filename, 'w') as fp:
            fp.write('%i\n' % smd.data.resolution)

    if do_plot:

        summaries, _ = split_map(get_summary, tts, mms)
        summaries_dict = dh.list_dicts(list(itertools.chain(*summaries)))
        plot_statistics(summaries_dict, 'cleaned')

    if do_plot_tuples:

        for i, (t, m) in enumerate(zip(itertools.chain(*tts), itertools.chain(*mms))):

            summary = get_summary(t)

            plt.figure()
            colors = dh.get_color_range(summary['n_parts'])
            plot_tuples(t, colors)

            print('open', smd.io.write_midi(*tuple(zip(*t))[1:], filename='/tmp/cleaned.mid' % i, tempo=m))
            plt.show()

    if do_plot:

        plt.show()



main = gh.auto_parser_inspector(allow_missing=True)(raw_main)

if __name__ == '__main__':

    main()
