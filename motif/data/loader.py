# this file is a hacked version of smd.data to handle output paths from cleaner.py. these should ideally be combined.

import pandas as pd
import numpy as np
import smd.data
import time

split_names = ('train', 'test', 'valid')
columns = ('file_index', 't0', 't1', 'midi', 'part')
column2index = {k: v for v, k in enumerate(columns)}
FILE_INDEX = column2index['file_index']
T0 = column2index['t0']
T1 = column2index['t1']
MIDI = column2index['midi']
PART = column2index['part']

_filename = '%s/cleaned_%s.csv'
_tempo_filename = '%s/cleaned_%s_tempo.csv'
tempo_columns = ('file_index', 'tempo')


def _load_df(path, split, tempo=False):

    with open('%s/resolution.txt' % path, 'r') as fp:
        resolution = int(fp.readline().strip())
    assert resolution == smd.data.resolution

    assert split in split_names, split
    if tempo:
        fn = _tempo_filename
        cols = tempo_columns
    else:
        fn = _filename
        cols = columns
    filename = fn % (path, split)
    for extension in ('', '.gz'):
        try:
            fn = filename + extension
            t0 = time.time()
            df = pd.read_csv(fn, dtype={'file_index': int, 't0': int, 't1': int, 'midi': int, 'part': int})
            print('read %s in %6.1f seconds' % (fn, time.time() - t0))
            break
        except:
            pass
    assert tuple(df.columns) == cols
    return df


def _load_np_generator(*args, **kwargs):
    df = _load_df(*args, **kwargs)
    mat = df.values
    difft = np.diff(df.file_index)
    assert np.all(difft >= 0)
    inds = np.concatenate(([0], np.where(difft == 1)[0] + 1, [mat.shape[0]]))
    return (mat[i:j, :] for i, j in zip(inds, inds[1:]))


def load_tuples(*args, **kwargs):
    '''
    Example:
    t = load_tuples("mus", "test")
    len(t) = number of files
    len(t[0]) = number of notes in the first file
    t[0][0] = the first event of the first file, with values corresponding to smd.data.columns
    '''
    return [tuple(zip(*this_mat.T)) for this_mat in _load_np_generator(*args, **kwargs)]


def load_tempos(*args, **kwargs):
    '''
    Example:
    d = load_tempos("mus", "train")
    '''
    return dict([_[0] for _ in load_tuples(*args, **kwargs, tempo=True)])


if __name__ == '__main__':

    from gridtools import helpers

    roots = ['~/output/cleaner/']
    hosts = ['bach.nexus.csiro.au', 'bracewell.hpc.csiro.au']
    for root in roots:
        for host in hosts:
            helpers.rsync(root, host, upload=True, verbose=True)